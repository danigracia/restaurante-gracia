const Plato = require("../models/Plato")
const { validationResult } = require("express-validator");

exports.obtenerPlatos = async (req, res) => {
    try {
        const platos = await Plato.find({}).sort({ tipo: "asc" })
        res.json(platos)
    } catch (error) {
        console.log(error)
        res.status(500).send("Hubo un error en el servidor")
    }

}

//Obtener 1 plato por su id
exports.obtenerPlato = async (req, res) => {
    try {
        const plato = await Plato.findById(req.params.id)
        
        if(!plato){
            return res.status(404).send({error: "No existe ningún plato con este id"})
        }

        res.json(plato)
    } catch (error) {
        console.log(error)
        res.status(500).send("Hubo un error en el servidor")
    }
}

exports.crearPlato = async (req, res) => {
    //Mostrar errores de express validator
    const errores = validationResult(req);
    if (!errores.isEmpty()) {
        return res.status(422).json({ errores: errores.array() });
    }

    try {
        //Crear plato
        const plato = await new Plato(req.body)
        plato.save()

        res.json(plato)

    } catch (error) {
        console.log(error)
        res.status(500).send("Hubo un error en el servidor")
    }


}

//Actualizar un plato
exports.actualizarPlato = async (req, res) => {
    //Mostrar errores de express validator
    const errores = validationResult(req);
    if (!errores.isEmpty()) {
        return res.status(422).json({ errores: errores.array() });
    }

    try {
        //Comprobar que exista el plato
        const existe = await Plato.findById(req.params.id)
        if(!existe) {
            return res.json({error: "No existe ningún plato con este id"})
        }

        //Actualizar
        const plato = await Plato.findOneAndUpdate({ _id: req.params.id }, {$set: req.body}, {new: true})
        res.json(plato)

    } catch (error) {
        console.log(error)
        res.status(500).send("Hubo un error en el servidor")
    }
}

exports.eliminarPlato = async (req, res) => {
    try {
        //Comprobar que exista el plato
        const existe = await Plato.findById(req.params.id)
        if(!existe) {
            return res.json({error: "No existe ningún plato con este id"})
        }

        //Actualizar
        await Plato.findOneAndRemove({ _id: req.params.id })
        res.json({msg: "Plato eliminado con exito"})

    } catch (error) {
        console.log(error)
        res.status(500).send("Hubo un error en el servidor")
    }
}
