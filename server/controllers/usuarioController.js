const Usuario = require("../models/Usuario");
const bcrypt = require("bcrypt");
const { validationResult } = require("express-validator");

exports.crearUsuario = async (req, res) => {
    //Mostrar errores de express validator
    const errores = validationResult(req);
    if (!errores.isEmpty()) {
        return res.status(422).json({ errores: errores.array() });
    }

    //Verificar si el usuario ya existe
    const existe = await Usuario.findOne({ email: req.body.email });
    if (existe) {
        return res
            .status(409)
            .send({ error: "Ya existe un usuario con este email" });
    }

    //Crear el nuevo usuario
    const usuario = await new Usuario(req.body);

    //Hashear el password
    usuario.password = bcrypt.hashSync(req.body.password, 10);

    try {
        usuario.save();
        res.json({ msg: "Usuario creado correctamente" });
    } catch (error) {
        console.log(error);
        res.status(500).send({ error: "Hubo un error en el servidor" });
    }
};
