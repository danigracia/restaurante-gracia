const Usuario = require("../models/Usuario");
const { validationResult } = require("express-validator");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
require("dotenv").config({ path: "variables.env" });

exports.login = async (req, res) => {
    //Mostrar errores de express validator
    const errores = validationResult(req);
    if (!errores.isEmpty()) {
        return res.status(401).json({ errores: errores.array() });
    }

    try {
        //Verificar si el usuario existe
        const usuario = await Usuario.findOne({ email: req.body.email });
        if (!usuario) {
            return res
                .status(401)
                .send({ error: "No existe ningún usuario con este email" });
        }

        //Validar password
        if (!bcrypt.compareSync(req.body.password, usuario.password)) {
            return res.status(401).send({ error: "El password es incorrecto" });
        }

        //Generar JWT
        const token = jwt.sign(
            {
                id: usuario._id,
                nombre: usuario.nombre,
                email: usuario.email,
            },
            process.env.SECRET_KEY,
            {
                expiresIn: "8h",
            }
        );

        res.json({ token });
    } catch (error) {
        console.log(error);
        res.status(500).send({ error: "Hubo un error en el servidor" });
    }
};
