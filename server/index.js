const express = require("express");
const cors = require("cors");
const conectarDB = require("./config/db");

//Crear el servidor
app = express();

//Conectar a la DB
conectarDB();

//Habilitar cors
app.use(cors());

//Habilitar json para los req
app.use(express.json());

//Habilitar carpeta publica
app.use(express.static("uploads"));

//Rutas de la app
app.use("/usuarios", require("./routes/usuarios"));
app.use("/auth", require("./routes/auth"));
app.use("/platos", require("./routes/platos"));

//Puerto de la app
const PORT = process.env.PORT || 4000;

//Iniciar la app
app.listen(PORT, () => {
    console.log(`Servidor corriendo en el puerto ${PORT}`);
});
