const jwt = require("jsonwebtoken");
require("dotenv").config();

const auth = (req, res, next) => {
    const authHeader = req.get("Authorization");
    if (authHeader) {
        const token = authHeader.split(" ")[1];

        if (token) {
            //Verificar token
            try {
                const usuario = jwt.verify(token, process.env.SECRET_KEY);
                req.usuario = usuario;
                next();
            } catch (error) {
                return res.status(403).send({error: "El token no es válido"})
            }
        }
    } else {
        return res.status(403).send({error: "No hay token"})
    }

    
};

module.exports = auth;
