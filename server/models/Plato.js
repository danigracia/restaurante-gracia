const mongoose = require("mongoose")

const platoSchema = mongoose.Schema({
    nombre: {
        type: String,
        required: true,
        trim: true,
    },
    ingredientes: {
        type: String,
        required: true,
        trim: true,
    },
    precio: {
        type: Number,
        required: true,
    },
    tipo: {
        type: Number,
        required: true,
        min: 1,
        max: 3,
    }
})

module.exports = mongoose.model("Plato", platoSchema) 