const router = require("express").Router();
const { check } = require("express-validator");
const auth = require("../middlewares/auth");
const platoController = require("../controllers/platoController")

const validaciones = [
    check("nombre", "El nombre es obligatorio").not().isEmpty(),
    check("ingredientes", "Los ingredientes son obligatorios").not().isEmpty(),
    check("precio", "El precio es obligatorio").not().isEmpty(),
    check("tipo", "El tipo de plato es obligatorio").not().isEmpty(),
    check("tipo", "El tipo de plato no es válido").isFloat({ min: 1, max: 3 }),
]

//Listar todos los platos
router.get("/", platoController.obtenerPlatos)

//Obtener plato por id
router.get("/:id", platoController.obtenerPlato)

/* Rutas privadas */

//Crear plato
router.post("/", auth, validaciones, platoController.crearPlato);

//Actualizar plato por id
router.put("/:id", auth, validaciones, platoController.actualizarPlato)

//Eliminar plato por id
router.delete("/:id", auth, platoController.eliminarPlato)

module.exports = router;
