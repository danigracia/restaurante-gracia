const router = require("express").Router();
const { check } = require("express-validator");
const auth = require("../middlewares/auth");
const authController = require("../controllers/authController");

//Login
router.post(
    "/",
    [
        check("email", "El email no es válido").isEmail(),
        check("password", "La contraseña es obligatoria").not().isEmpty(),
    ],
    authController.login
);

router.get("/", auth, (req, res) => {
    if (req.usuario) {
        res.json(req.usuario);
    } else {
        res.status(403);
    }
});

module.exports = router;
