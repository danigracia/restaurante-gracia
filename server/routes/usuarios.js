const router = require("express").Router()
const { check } = require("express-validator")
const usuarioController = require("../controllers/usuarioController")

router.post("/", 
[
    check("nombre", "El nombre es obligatorio").not().isEmpty(),
    check("email", "El email no es válido").isEmail(),
    check("password", "La contraseña debe ser de al menos 6 caracteres").isLength({min: 6})
],
usuarioController.crearUsuario)

module.exports = router