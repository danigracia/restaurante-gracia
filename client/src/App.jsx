import { BrowserRouter, Routes, Route } from "react-router-dom";

import Layout from "./layout/Layout";
import Inicio from "./pages/Inicio";
import Menu from "./pages/Menu";

function App() {
    return (
        <BrowserRouter>
            <Routes>
                <Route path="/" element={<Layout />}>
                    <Route index element={<Inicio />} />
                    <Route path="/menu" element={<Menu />} />
                </Route>
            </Routes>
        </BrowserRouter>
    );
}

export default App;
