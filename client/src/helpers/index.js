export const fechaHoy = () => {
    const today = new Date(Date.now());
    const options = {
        weekday: "long",
        month: "long",
        day: "numeric",
    };

    return today.toLocaleDateString("es-ES", options)
}

export const formatearEuro = (cantidad) => {
    const euro = cantidad.toLocaleString('es-ES', {style: 'currency',currency: 'EUR', minimumFractionDigits: 2})
    return euro
}