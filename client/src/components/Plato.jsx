import React from "react";

import { formatearEuro } from "../helpers";

export default function Plato({ plato }) {
    const { nombre, ingredientes, precio } = plato;

    return (
        <li className="py-2 px-8 mb-2 bg-gray-200 rounded-lg">
            <div className="flex justify-between">
                <h3 className="text-gray-700 font-bold">{nombre}</h3>
                <p>{formatearEuro(precio)}</p>
            </div>
            <p className="text-sm text-gray-500">{ingredientes}</p>
        </li>
    );
}
