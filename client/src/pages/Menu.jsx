import React, { useState, useEffect } from "react";

import { fechaHoy } from "../helpers";
import clienteAxios from "../config/axios";

import Plato from "../components/Plato";

export default function Menu() {
    const [platos, setPlatos] = useState([]);

    useEffect(() => {
        const consultarApi = async () => {
            const respuesta = await clienteAxios.get("/platos");
            setPlatos(respuesta.data);
        };

        consultarApi();
    }, []);

    return (
        <>
            <h1 className="text-4xl text-center text-gray-800">
                Menu {fechaHoy()}
            </h1>

            {/* Menu */}
            <div className="max-w-md mx-auto">
                {/* Primeros */}
                <div className="my-8 flex flex-col items-center">
                    <h2 className="text-2xl text-gray-700 mb-4">Primeros</h2>
                    <ul className="w-full">
                        {platos
                            .filter((plato) => plato.tipo === 1)
                            .map((plato) => (
                                <Plato key={plato._id} plato={plato} />
                            ))}
                    </ul>
                </div>

                {/* Segundos */}
                <div className="my-8 flex flex-col items-center w-full">
                    <h2 className="text-2xl text-gray-700 mb-4">Segundos</h2>
                    <ul className="w-full">
                        {platos
                            .filter((plato) => plato.tipo === 2)
                            .map((plato) => (
                                <Plato key={plato._id} plato={plato} />
                            ))}
                    </ul>
                </div>

                {/* Postres */}
                <div className="my-8 flex flex-col items-center">
                    <h2 className="text-2xl text-gray-700 mb-4">Postres</h2>
                    <ul className="w-full">
                        {platos
                            .filter((plato) => plato.tipo === 3)
                            .map((plato) => (
                                <Plato key={plato._id} plato={plato} />
                            ))}
                    </ul>
                </div>
            </div>
        </>
    );
}
