import React from "react";
import Imagen from "../img/hero.jpg";

export default function Hero() {
    return (
        <div
            className="hero h-screen"
            style={{
                backgroundImage: `url(${Imagen})`,
            }}
        >
            <div className="hero-overlay bg-opacity-60"></div>
            <div className="text-center hero-content text-neutral-content">
                <div>
                    <h1 className="mb-5 text-9xl font-mono font-bold">
                        PIZZA
                        <br />
                        ESPACIAL
                    </h1>
                    <button className="btn btn-primary text-xl btn-xl">
                        ¡Reservar ahora!
                    </button>
                </div>
            </div>
        </div>
    );
}
