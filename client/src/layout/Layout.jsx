import React from "react";
import { Outlet, useLocation } from "react-router-dom";

import NavBar from "./NavBar";
import Hero from "./Hero";

export default function Layout() {
    const location = useLocation();

    return (
        <div>
            <NavBar />
            {location.pathname === "/" && <Hero />}
            <main className="mt-8">
                <Outlet />
            </main>
        </div>
    );
}
