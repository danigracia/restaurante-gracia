import React from "react";
import { Link } from "react-router-dom";

export default function NavBar() {
    return (
        <div className="py-2 shadow-lg bg-neutral text-neutral-content">
            <div className="container mx-auto navbar justify-between">
                <h2 className="md:text-3xl text-2xl text-center text-gray-100 font-bold">
                    Casa<span className="text-secondary">Gracia</span>
                </h2>

                <div className="items-stretch hidden lg:flex">
                    <Link to="/" className="btn btn-ghost rounded-btn">
                        Inico
                    </Link>
                    <Link to="/" className="btn btn-ghost rounded-btn">
                        ¿Quienes Somos?
                    </Link>
                    <Link to="/menu" className="btn btn-ghost rounded-btn">
                        Menu
                    </Link>
                    <Link to="/reservar" className="btn btn-ghost rounded-btn">
                        Reservar
                    </Link>
                </div>
            </div>
        </div>
    );
}
