import React, { useContext } from "react";
import { Link } from "react-router-dom";
import platoContext from "../../context/platos/platoContext";
import Swal from "sweetalert2/dist/sweetalert2.all.js";

export default function Plato({ plato }) {
    const { nombre, precio, tipo } = plato;

    const PlatoContext = useContext(platoContext);
    const { eliminarPlato } = PlatoContext;

    const handleEliminar = () => {
        Swal.fire({
            title: "Estas seguro?",
            text: "No podras revertir esto!",
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: "#009485",
            cancelButtonColor: "#ff5724",
            confirmButtonText: "Si, eliminar!",
        }).then((result) => {
            if (result.isConfirmed) {
                eliminarPlato(plato._id);
            }
        });
    };

    return (
        <tr>
            <td className="p-4">{nombre}</td>
            <td className="p-4">{precio}</td>
            <td className="p-4 md:table-cell hidden">
                {tipo === 1 ? "Primero" : tipo === 2 ? "Segundo" : "Postre"}
            </td>
            <td className="p-4 flex flex-col md:flex-row gap-2">
                <Link
                    to={`/admin/platos/${plato._id}`}
                    className="btn btn-success"
                >
                    Editar
                </Link>
                <button
                    className="btn btn-error"
                    onClick={() => handleEliminar()}
                >
                    Eliminar
                </button>
            </td>
        </tr>
    );
}
