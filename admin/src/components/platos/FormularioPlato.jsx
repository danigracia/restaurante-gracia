import React, { useContext } from "react";
import { useNavigate } from "react-router-dom";
import { useFormik } from "formik";
import * as Yup from "yup";

import platoContext from "../../context/platos/platoContext";

export default function FormularioPlato() {
    //Redireccionar
    const history = useNavigate();

    //Plato Context
    const PlatoContext = useContext(platoContext);
    const { platoactual, agregarPlato, editarPlato } = PlatoContext;

    //Formik
    const formik = useFormik({
        initialValues: {
            nombre: platoactual.nombre || "",
            ingredientes: platoactual.ingredientes || "",
            precio: platoactual.precio || "",
            tipo: platoactual.tipo || "",
        },
        validationSchema: Yup.object({
            nombre: Yup.string().required("El nombre del plato es obligatorio"),
            ingredientes: Yup.string().required(
                "Los ingredientes son obligatorios"
            ),
            precio: Yup.number().required("El precio es obligatorio"),
            tipo: Yup.number()
                .required("El tipo de plato es obligatorio")
                .min(1, "El plato puede ser minimo un primero")
                .max(3, "El plato no puede ser posterior al postre"),
        }),
        onSubmit: async (valores) => {
            if (platoactual) {
                //Editando existente
                await editarPlato(valores);
            } else {
                //Creando
                await agregarPlato(valores);
            }

            //Redireccionar
            history("/admin/platos");
        },
    });

    return (
        <form className="md:w-1/2 mx-auto mt-6" onSubmit={formik.handleSubmit}>
            <div className="form-control">
                <label htmlFor="nombre" className="label">
                    Nombre *
                </label>
                <input
                    type="text"
                    name="nombre"
                    className={`input input-bordered ${
                        formik.errors.nombre && "input-error"
                    }`}
                    value={formik.values.nombre}
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                />

                {formik.errors.nombre && (
                    <p className="text-red-500 text-sm">
                        {formik.errors.nombre}
                    </p>
                )}
            </div>

            <div className="form-control">
                <label htmlFor="ingredientes" className="label">
                    Ingredientes *
                </label>
                <textarea
                    type="number"
                    name="ingredientes"
                    className={`textarea textarea-bordered ${
                        formik.errors.ingredientes && "textarea-error"
                    }`}
                    value={formik.values.ingredientes}
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                ></textarea>

                {formik.errors.ingredientes && (
                    <p className="text-red-500 text-sm">
                        {formik.errors.ingredientes}
                    </p>
                )}
            </div>

            <div className="form-control">
                <label htmlFor="precio" className="label">
                    Precio *
                </label>
                <input
                    type="number"
                    name="precio"
                    className={`input input-bordered ${
                        formik.errors.nombre && "input-error"
                    }`}
                    value={formik.values.precio}
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                />

                {formik.errors.precio && (
                    <p className="text-red-500 text-sm">
                        {formik.errors.precio}
                    </p>
                )}
            </div>

            <div className="form-control">
                <label htmlFor="tipo" className="label">
                    Tipo *
                </label>
                <select
                    type="number"
                    name="tipo"
                    className={`select select-bordered ${
                        formik.errors.nombre && "select-error"
                    }`}
                    value={formik.values.tipo}
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                >
                    <option value="" disabled>
                        -- Selecciona --
                    </option>
                    <option value="1">Primero</option>
                    <option value="2">Segundo</option>
                    <option value="3">Postre</option>
                </select>

                {formik.errors.tipo && (
                    <p className="text-red-500 text-sm">{formik.errors.tipo}</p>
                )}
            </div>

            {/* Submit */}
            <input
                type="submit"
                className="btn btn-primary btn-block mt-8"
                value={platoactual ? "Guardar Cambios" : "Añadir el plato"}
            />
        </form>
    );
}
