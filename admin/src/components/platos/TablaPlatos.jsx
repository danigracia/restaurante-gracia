import React, { useContext, useEffect } from "react";
import platoContext from "../../context/platos/platoContext";
import Spinner from "../ui/Spinner";
import Plato from "./Plato";

export default function TablaPlatos() {
    const PlatoContext = useContext(platoContext);
    const { platos, cargando, obtenerPlatos } = PlatoContext;

    useEffect(() => {
        obtenerPlatos();
    }, []);

    return cargando ? (
        <Spinner />
    ) : (
        <table className="table-fixed bg-white">
            <thead>
                <tr className="bg-gray-800 text-white uppercase">
                    <th className="p-4">Nombre</th>
                    <th className="p-4">Precio</th>
                    <th className="p-4 md:table-cell hidden">Tipo</th>
                    <th className="p-4">Acciones</th>
                </tr>
            </thead>
            <tbody>
                {platos.map((plato) => (
                    <Plato key={plato._id} plato={plato} />
                ))}
            </tbody>
        </table>
    );
}
