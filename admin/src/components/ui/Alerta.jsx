import React from "react";

export default function Alerta({ alerta }) {
    const { mensaje, tipo } = alerta;

    return (
        <div
            className={`alert ${
                tipo === "error" ? "text-white bg-primary" : "alert-success"
            } justify-center`}
        >
            <p className="uppercase font-bold">{mensaje}</p>
        </div>
    );
}
