import React, { useContext } from "react";

import platoContext from "../../context/platos/platoContext";
import FormularioPlato from "../../components/platos/FormularioPlato";
import Alerta from "../../components/ui/Alerta";

export default function CrearPlato() {
    const PlatoContext = useContext(platoContext);
    const { alerta } = PlatoContext;

    return (
        <>
            <h2 className="text-gray-800 text-center text-4xl font-bold mb-10">
                Nuevo Plato
            </h2>
            {alerta && <Alerta alerta={alerta} />}
            <FormularioPlato />
        </>
    );
}
