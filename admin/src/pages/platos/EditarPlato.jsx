import React, { useContext, useEffect } from "react";
import { useParams } from "react-router-dom";

import platoContext from "../../context/platos/platoContext";

import FormularioPlato from "../../components/platos/FormularioPlato";
import Alerta from "../../components/ui/Alerta";

export default function CrearPlato() {
    const { id } = useParams();

    const PlatoContext = useContext(platoContext);
    const { platoactual, alerta, obtenerPlato } = PlatoContext;

    useEffect(() => {
        obtenerPlato(id);
    }, []);

    return (
        <>
            <h2 className="text-gray-800 text-center text-4xl font-bold mb-10">
                Editar Plato
            </h2>
            {alerta && <Alerta alerta={alerta} />}

            {platoactual && <FormularioPlato />}
        </>
    );
}
