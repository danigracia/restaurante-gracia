import React, { useContext } from "react";
import { Link } from "react-router-dom";
import Layout from "../../layout/Layout";

//Context
import platoContext from "../../context/platos/platoContext";

//Components
import Alerta from "../../components/ui/Alerta";
import TablaPlatos from "../../components/platos/TablaPlatos";

export default function Menu() {
    const PlatoContext = useContext(platoContext);
    const { alerta } = PlatoContext;

    return (
        <div className="flex flex-col items-center">
            <h2 className="text-gray-800 text-center text-4xl font-bold mb-10">
                Administrar Menu
            </h2>
            <Link to={"/admin/platos/crear"} className="btn btn-primary mb-5">
                Crear
            </Link>
            {alerta && <Alerta alerta={alerta} />}
            <TablaPlatos />
        </div>
    );
}
