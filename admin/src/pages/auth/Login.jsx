import { useContext, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import { useFormik } from "formik";
import * as Yup from "yup";
import authContext from "../../context/auth/authContext";
import Alerta from "../../components/ui/Alerta";

export default function Login() {
    let history = useNavigate();

    const AuthContext = useContext(authContext);
    const { autenticado, alerta, iniciarSesion } = AuthContext;

    //En caso de que el usuario se haya autenticado
    useEffect(() => {
        if (autenticado) {
            history("/admin");
        }

        //eslint-disable-next-line
    }, [autenticado]);

    const formik = useFormik({
        initialValues: {
            email: "",
            password: "",
        },
        validationSchema: Yup.object({
            email: Yup.string()
                .required("El email es obligatorio")
                .email("El email no es válido"),
            password: Yup.string().required("El password es obligatorio"),
        }),
        onSubmit: (valores) => {
            iniciarSesion(valores);
        },
    });

    return (
        <main className="h-screen bg-gray-200 flex flex-col items-center justify-center">
            {alerta && <Alerta alerta={alerta} />}
            <div className="mt-4 md:w-1/3 bg-white p-10 rounded-lg">
                <h1 className="font-bold text-primary text-2xl text-center uppercase">
                    Iniciar Sesión
                </h1>

                <form className="mt-4" onSubmit={formik.handleSubmit}>
                    <div className="form-control">
                        <label htmlFor="email" className="label">
                            Email
                        </label>
                        <input
                            type="email"
                            name="email"
                            id="email"
                            className="input input-bordered"
                            placeholder="Tu Email"
                            value={formik.values.email}
                            onChange={formik.handleChange}
                            onBlur={formik.handleBlur}
                        />
                    </div>

                    <div className="form-control">
                        <label htmlFor="password" className="label">
                            Password
                        </label>
                        <input
                            type="password"
                            name="password"
                            id="password"
                            className="input input-bordered"
                            placeholder="Tu Password"
                            value={formik.values.password}
                            onChange={formik.handleChange}
                            onBlur={formik.handleBlur}
                        />
                    </div>

                    <input
                        type="submit"
                        className="btn btn-primary btn-block mt-6"
                        value="Iniciar Sesión"
                    />
                </form>
            </div>
        </main>
    );
}
