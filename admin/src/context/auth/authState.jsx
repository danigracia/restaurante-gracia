import { useReducer } from "react";
import authReducer from "./authReducer";
import authContext from "./authContext";

import clienteAxios from "../../config/axios";
import tokenAuth from "../../config/tokenAuth";

import {
    LOGIN_EXITOSO,
    LOGIN_ERROR,
    OBTENER_USUARIO,
    CERRAR_SESSION,
} from "./authTypes";

const AuthState = (props) => {
    const initialState = {
        token: localStorage.getItem("token"),
        autenticado: null,
        usuario: null,
        cargando: true,
        alerta: null,
    };

    const [state, dispatch] = useReducer(authReducer, initialState);

    //Funciones

    //Cuando el usuario inicia sesion
    const iniciarSesion = async (datos) => {
        try {
            const respuesta = await clienteAxios.post("/auth", datos);
            dispatch({
                type: LOGIN_EXITOSO,
                payload: respuesta.data,
            });

            //Obtener el usuario
            usuarioAutenticado();
        } catch (error) {
            dispatch({
                type: LOGIN_ERROR,
                payload: {
                    mensaje: error.response.data.error,
                    tipo: "error",
                },
            });
        }
    };

    //Retorna el usuario autenticado
    const usuarioAutenticado = async () => {
        const token = localStorage.getItem("token");
        if (token) {
            //Enviar token por headers
            tokenAuth(token);
        }

        try {
            const respuesta = await clienteAxios.get("/auth");
            dispatch({
                type: OBTENER_USUARIO,
                payload: respuesta.data,
            });
        } catch (error) {
            dispatch({
                type: LOGIN_ERROR,
                payload: {
                    mensaje: error.response.data.error,
                    tipo: "error",
                },
            });
        }
    };

    const cerrarSesion = () => {
        dispatch({
            type: CERRAR_SESSION,
        });
    };

    return (
        <authContext.Provider
            value={{
                token: state.token,
                autenticado: state.autenticado,
                usuario: state.usuario,
                cargando: state.cargando,
                alerta: state.alerta,
                iniciarSesion,
                usuarioAutenticado,
                cerrarSesion,
            }}
        >
            {props.children}
        </authContext.Provider>
    );
};

export default AuthState;
