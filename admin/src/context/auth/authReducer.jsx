import {
    LOGIN_EXITOSO,
    LOGIN_ERROR,
    OBTENER_USUARIO,
    CERRAR_SESSION,
} from "./authTypes";

const authReducer = (state, action) => {
    switch (action.type) {
        case LOGIN_EXITOSO:
            localStorage.setItem("token", action.payload.token);
            return {
                ...state,
                autenticado: true,
                alerta: null,
                cargando: false,
            };

        case OBTENER_USUARIO:
            return {
                ...state,
                autenticado: true,
                usuario: action.payload,
                cargando: false,
            };

        case CERRAR_SESSION:
        case LOGIN_ERROR:
            localStorage.removeItem("token");
            return {
                ...state,
                token: null,
                usuario: null,
                autenticado: null,
                alerta: action.payload,
                cargando: false,
            };

        default:
            return state;
    }
};

export default authReducer;
