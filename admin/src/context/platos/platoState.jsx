import React, { useReducer } from "react";
import platoContext from "./platoContext";
import platoReducer from "./platoReducer";
import * as types from "./platoTypes";

import clienteAxios from "../../config/axios";

const PlatoState = ({ children }) => {
    const initialState = {
        platos: [],
        platoactual: null,
        cargando: false,
        alerta: null,
    };

    //Crear state y dispatch
    const [state, dispatch] = useReducer(platoReducer, initialState);

    const mostrarAlerta = (alerta) => {
        dispatch({
            type: types.MOSTRAR_ALERTA,
            payload: alerta,
        });

        setTimeout(() => {
            dispatch({
                type: types.LIMPIAR_ALERTA,
            });
        }, 3000);
    };

    //Obtener los platos
    const obtenerPlatos = async () => {
        dispatch({
            type: types.OBTENER_PLATOS,
        });

        try {
            const respuesta = await clienteAxios.get("/platos");
            dispatch({
                type: types.OBTENER_PLATOS_EXITO,
                payload: respuesta.data,
            });
        } catch (error) {
            dispatch({
                type: types.OBTENER_PLATOS_ERROR,
                payload: {
                    mensaje: error.response.data,
                    tipo: "error",
                },
            });

            setTimeout(() => {
                dispatch({
                    type: types.LIMPIAR_ALERTA,
                });
            }, 3000);
        }
    };

    //Elimina un plato
    const eliminarPlato = async (id) => {
        try {
            await clienteAxios.delete(`/platos/${id}`);
            dispatch({
                type: types.ELIMINAR_PLATO_EXITO,
                payload: id,
            });
        } catch (error) {
            dispatch({
                type: types.ELIMINAR_PLATO_ERROR,
                payload: {
                    mensaje: error.response.data,
                    tipo: "error",
                },
            });

            setTimeout(() => {
                dispatch({
                    type: types.LIMPIAR_ALERTA,
                });
            }, 3000);
        }
    };

    //Agrega un plato
    const agregarPlato = async (plato) => {
        dispatch({
            type: types.AGREGAR_PLATO,
        });

        try {
            const respuesta = await clienteAxios.post(`/platos`, plato);
            dispatch({
                type: types.AGREGAR_PLATO_EXITO,
                payload: respuesta.data,
            });
        } catch (error) {
            dispatch({
                type: types.AGREGAR_PLATO_ERROR,
                payload: {
                    mensaje: error.response.data.error,
                    tipo: "error",
                },
            });

            setTimeout(() => {
                dispatch({
                    type: types.LIMPIAR_ALERTA,
                });
            }, 3000);
        }
    };

    //Obtiene un plato
    const obtenerPlato = async (id) => {
        dispatch({
            type: types.OBTENER_PLATO,
        });

        try {
            const respuesta = await clienteAxios.get(`/platos/${id}`);
            dispatch({
                type: types.OBTENER_PLATO_EXITO,
                payload: respuesta.data,
            });
        } catch (error) {
            dispatch({
                type: types.OBTENER_PLATO_ERROR,
                payload: {
                    mensaje: error.response.data.error,
                    tipo: "error",
                },
            });
        }
    };

    //Edita un plato
    const editarPlato = async (plato) => {
        dispatch({
            type: types.EDITAR_PLATO,
        });

        try {
            const respuesta = await clienteAxios.put(
                `/platos/${state.platoactual._id}`,
                plato
            );
            dispatch({
                type: types.EDITAR_PLATO_EXITO,
                payload: respuesta.data,
            });
        } catch (error) {
            dispatch({
                type: types.EDITAR_PLATO_ERROR,
                payload: {
                    mensaje: error.response.data.error,
                    tipo: "error",
                },
            });

            setTimeout(() => {
                dispatch({
                    type: types.LIMPIAR_ALERTA,
                });
            }, 3000);
        }
    };
    return (
        <platoContext.Provider
            value={{
                platos: state.platos,
                platoactual: state.platoactual,
                cargando: state.cargando,
                alerta: state.alerta,
                mostrarAlerta,
                obtenerPlatos,
                eliminarPlato,
                agregarPlato,
                obtenerPlato,
                editarPlato,
            }}
        >
            {children}
        </platoContext.Provider>
    );
};

export default PlatoState;
