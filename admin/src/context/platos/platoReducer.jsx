import * as types from "./platoTypes";

const menuReducer = (state, action) => {
    switch (action.type) {
        case types.OBTENER_PLATOS:
        case types.AGREGAR_PLATO:
        case types.OBTENER_PLATO:
        case types.EDITAR_PLATO:
            return {
                ...state,
                cargando: true,
            };

        //Obtiene el listado de platos
        case types.OBTENER_PLATOS_EXITO:
            return {
                ...state,
                platos: action.payload,
                cargando: false,
            };

        //Elimina un plato
        case types.ELIMINAR_PLATO_EXITO:
            return {
                ...state,
                platos: state.platos.filter(
                    (plato) => plato._id !== action.payload
                ),
            };

        //Agrega un plato
        case types.AGREGAR_PLATO_EXITO:
            return {
                ...state,
                platos: [action.payload, ...state.platos],
            };

        //Obtiene un plato actual
        case types.OBTENER_PLATO_EXITO:
            return {
                ...state,
                platoactual: action.payload,
            };

        //Edita un plato
        case types.EDITAR_PLATO_EXITO:
            return {
                ...state,
                platoactual: null,
                platos: state.platos.map((plato) =>
                    plato._id === action.payload._id ? action.payload : plato
                ),
            };

        //Errores
        case types.OBTENER_PLATOS_ERROR:
        case types.ELIMINAR_PLATO_ERROR:
        case types.AGREGAR_PLATO_ERROR:
        case types.OBTENER_PLATO_ERROR:
        case types.EDITAR_PLATO_ERROR:
        case types.MOSTRAR_ALERTA:
            return {
                ...state,
                cargando: false,
                alerta: action.payload,
            };

        //Alertas
        case types.LIMPIAR_ALERTA:
            return {
                ...state,
                alerta: null,
            };

        default:
            return state;
    }
};

export default menuReducer;
