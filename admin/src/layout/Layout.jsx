import { useContext } from "react";
import { Link, useLocation, useNavigate } from "react-router-dom";
import authContext from "../context/auth/authContext";

const Layout = ({ children }) => {
    const AuthContext = useContext(authContext);
    const { cerrarSesion } = AuthContext;

    const location = useLocation();
    const urlActual = location.pathname;

    const history = useNavigate();

    const handleLogout = () => {
        cerrarSesion();
        history("/");
    };

    return (
        <div className="md:flex md:min-h-screen">
            <div className="lg:w-1/6 md:w-1/3 bg-gray-800 px-5 py-10">
                <div>
                    <h2 className="md:text-3xl text-2xl text-center text-gray-100 font-bold">
                        Casa<span className="text-primary">Gracia</span>
                    </h2>
                    <h3 className="text-xl text-gray-100 text-center font-bold mt-2">
                        Administración
                    </h3>

                    {/* Acciones */}
                    <div className="mt-4 flex justify-center">
                        <button
                            className="btn btn-primary btn-sm"
                            onClick={() => handleLogout()}
                        >
                            <i className="fas fa-power-off"></i>
                        </button>
                    </div>
                </div>

                <nav className="mt-4 pt-4 border-t-4 border-t-gray-400">
                    <Link
                        to="/admin"
                        className={`${
                            urlActual === "/admin"
                                ? "bg-primary hover:bg-primary-focus text-primary-content"
                                : ""
                        } p-3 text-gray-800 text-center font-bold bg-gray-100 uppercase rounded-lg block hover:bg-gray-300 mt-4`}
                    >
                        Inicio
                    </Link>
                    <Link
                        to="/admin/platos"
                        className={`${
                            urlActual === "/admin/platos"
                                ? "bg-primary hover:bg-primary-focus text-primary-content"
                                : ""
                        } p-3 text-gray-800 text-center font-bold bg-gray-100 uppercase rounded-lg block hover:bg-gray-300 mt-4`}
                    >
                        Menu
                    </Link>
                </nav>
            </div>
            <main className="lg:w-5/6 md:w-2/3 px-10 py-20 md:h-screen bg-gray-200">
                {children}
            </main>
        </div>
    );
};

export default Layout;
