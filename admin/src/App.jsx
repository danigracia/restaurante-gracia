import { BrowserRouter, Routes, Route } from "react-router-dom";

import Inicio from "./pages/Inicio";

//Auth
import Login from "./pages/auth/Login";
import PrivateRoute from "./PrivateRoute";

//Platos
import Platos from "./pages/platos/Platos";
import CrearPlato from "./pages/platos/CrearPlato";
import EditarPlato from "./pages/platos/EditarPlato";

//Context
import AuthState from "./context/auth/authState";
import PlatoState from "./context/platos/platoState";

function App() {
    return (
        <AuthState>
            <PlatoState>
                <BrowserRouter>
                    <Routes>
                        <Route path="/" element={<Login />} />

                        {/* Rutas Privadas */}
                        <Route path="/admin" element={<PrivateRoute />}>
                            <Route index element={<Inicio />} />

                            {/* Platos */}
                            <Route path="platos" element={<Platos />} />
                            <Route
                                path="platos/crear"
                                element={<CrearPlato />}
                            />
                            <Route
                                path="platos/:id"
                                element={<EditarPlato />}
                            />
                        </Route>
                    </Routes>
                </BrowserRouter>
            </PlatoState>
        </AuthState>
    );
}

export default App;
